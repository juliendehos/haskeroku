{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import           Data.Aeson (ToJSON)
import qualified Clay as C
import           Control.Monad.Trans (liftIO)
import qualified Data.ByteString.Char8 as B8
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Database.PostgreSQL.Simple as P
import           Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import           GHC.Generics (Generic)
import           Lucid
import           Network.Wai.Middleware.Cors (simpleCors)
import           System.Environment (getEnv)
import           Web.Scotty (get, html, json, middleware, scotty)

data Music = Music T.Text T.Text deriving (Show, Generic)

instance FromRow Music where fromRow = Music <$> field <*> field

instance ToJSON Music

getMusics :: String -> IO [Music]
getMusics params = do
    conn <- P.connectPostgreSQL $ B8.pack params
    musics <- P.query_ conn "SELECT artists.name, titles.name FROM artists \
                            \ INNER JOIN titles ON artists.id = titles.artist"
    P.close conn
    return musics

bodyCss :: C.Css
bodyCss = C.body C.? C.backgroundColor  C.beige

main :: IO ()
main = do
    dbParams <- getEnv "DATABASE_URL"
    portStr <- getEnv "PORT"
    let port = read portStr

    scotty port $ do

        middleware simpleCors

        get "/" $ do
            musics <- liftIO $ getMusics dbParams
            let formatMusic :: Music -> T.Text
                formatMusic (Music artist title) = 
                    T.concat [artist, T.pack " - ", title]
            html $ renderText $ doctypehtml_ $ do
                header_ $ do
                    title_ "Haskeroku"
                    style_ $ L.toStrict $ C.render bodyCss
                body_ $ do
                    h1_ "Haskeroku"
                    div_ $ do
                        "My music:"
                        ul_ $ mapM_ (li_ . toHtml . formatMusic) musics
                    p_ $ a_ [href_ "raw"] "get raw data"
                    p_ $ a_ [href_ "json"] "get json data"
                    p_ $ a_ [href_ "https://gitlab.com/juliendehos/haskeroku"] 
                        "go to repository"

        get "/raw" $ liftIO (getMusics dbParams) 
            >>= html . L.fromStrict . T.pack . show 

        get "/json" $ liftIO (getMusics dbParams) >>= json

