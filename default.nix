{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:
let drv = pkgs.haskellPackages.callCabal2nix "haskeroku" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

