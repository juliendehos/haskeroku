
# Deploy a Haskell/PostgreSQL application on Heroku

See the deployed application here : <https://haskeroku.herokuapp.com/>.


## Setup

- install `postqresql`

- install `heroku-cli` (NixOS): `nix-env -iA nixos.heroku`

- install `heroku-cli` (other OS): <https://devcenter.heroku.com/articles/heroku-cli>


## Deploy on Heroku

- get a free account on [Heroku](https://www.heroku.com/)

- connect to heroku, using heroku-cli:

```
heroku login
```

- create a heroku app, **from the git repository of the project**:

```
heroku create --buildpack https://github.com/mfine/heroku-buildpack-stack.git
```

- create and initialize the postgresql database:
```
heroku addons:create heroku-postgresql:hobby-dev
heroku psql -f music.sql
```

- deploy and open the application on heroku:

```
git push heroku master
heroku open
```

- disconnect from heroku:

```
heroku logout
```


## Test locally

- with NixOS:

```
PORT=3000 DATABASE_URL=`heroku config:get DATABASE_URL` nix-shell --run "cabal run"
```

- with Stack:

```
stack setup
stack build
PORT=3000 DATABASE_URL=`heroku config:get DATABASE_URL` stack exec haskeroku
```

## How it works...

- project source code :
    - haskell source code: [Main.hs](Main.hs)
    - cabal configuration: [haskeroku.cabal](haskeroku.cabal)
    - database initialization: [music.sql](music.sql)

- haskell build toolchain:
    - for nixos: [default.nix](default.nix)
    - or for stack: [stack.yaml](stack.yaml)

- continuous integration with gitlab: [.gitlab-ci.yml](.gitlab-ci.yml)

- deployment on heroku: [Procfile](Procfile)


